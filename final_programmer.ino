/* Author : Martin Fonteyn
 * Date   : 22/07/23
 */

const char ADDRESS_PINS_ROM[] = { 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 18}; // of the ROM, A0 to A15
const char DATA_PINS[] = {23, 25, 27, 29, 31, 33, 35, 37};

#define NBR_ADDRESS 16   // SST39SF010A supports 17 addresses, 020A 18 addresses and 040 19 addresses. If you use more than 16, you have to abandon unsigned ints
#define WRITE_ENABLE A9  // low = enabled, write to flash
#define OUTPUT_ENABLE A8 // low = enabled, output from the flash
#define CHIP_ENABLE A7 // low = enable, flash
#define BUS_ENABLE A10 // low = disabled
#define READ_WRITE A13
#define REAL_A15 A12
#define BAUDRATE 9600

bool is_read_mode;
byte mode = 0; // 0 = not chosen yet, 1 = read, 2 = choose write start address, 3 = write, 4 = dump, 5 = choose check start address, 6 = check
byte received_byte;
unsigned int start_address, current_address, end_address;

void read_mode() {
  if (is_read_mode)
    return;

  for (short i = 0; i < 8; i++) pinMode(DATA_PINS[i], INPUT);
  delay(1);
  digitalWrite(OUTPUT_ENABLE, LOW);
  digitalWrite(WRITE_ENABLE, HIGH);
  delay(1);
  
  is_read_mode = true;
}

void write_mode() {
  if (!is_read_mode)
    return;

  for (short i = 0; i < 8; i++) pinMode(DATA_PINS[i], OUTPUT);
  delay(1);
  digitalWrite(WRITE_ENABLE, HIGH);
  digitalWrite(OUTPUT_ENABLE, HIGH);
  delay(1);
  
  is_read_mode = false;
}

void dump_some(unsigned int start_address, unsigned int end_address) {
  read_mode();
  byte read_buffer;
  
  for (unsigned int address = start_address; address < end_address; address++) {

    for (short i = 0; i < NBR_ADDRESS; i++) digitalWrite(ADDRESS_PINS_ROM[i], (address >> i) & 1);
    delayMicroseconds(1);
    
    read_buffer = 0;
    for (short i = 0; i < 8; i++) read_buffer += bit(i) * digitalRead(DATA_PINS[i]);
    
    Serial.print((char) read_buffer);
  }
}

void compare_some(unsigned int address, byte data) {
  read_mode();

  for (short i = 0; i < NBR_ADDRESS; i++) digitalWrite(ADDRESS_PINS_ROM[i], (address >> i) & 1);
  delayMicroseconds(1);
  
  byte read_buffer = 0;
  for (short i = 0; i < 8; i++) read_buffer += bit(i) * digitalRead(DATA_PINS[i]);

  if (read_buffer != data) {
    Serial.print("Incorrect data at address "); Serial.println(address, HEX);
  }
  
}

void read_some(unsigned int start_address, unsigned int end_address) {

  read_mode();
  byte read_buffer;
  char buf[30];
  
  for (unsigned int address = start_address; address < end_address; address++) {
    if (address % 32 == 0) {
      Serial.println("");
      sprintf(buf, "0x%04x %06d : ", address, address);
      Serial.print(buf);
    }
    else if (address % 8 == 0) {
      Serial.print("| ");
    }
    
    for (short i = 0; i < NBR_ADDRESS; i++) digitalWrite(ADDRESS_PINS_ROM[i], (address >> i) & 1);
    delayMicroseconds(1);
    
    read_buffer = 0;
    for (short i = 0; i < 8; i++) read_buffer += bit(i) * digitalRead(DATA_PINS[i]);
    
    Serial.print(read_buffer, HEX); Serial.print(" ");
  }
}

void send_command(unsigned int address, byte data) {
  digitalWrite(WRITE_ENABLE, HIGH);
  delayMicroseconds(20);

  // Write address
  for (short i = 0; i < NBR_ADDRESS; i++) digitalWrite(ADDRESS_PINS_ROM[i], (address >> i) & 1);
  delayMicroseconds(20);
  digitalWrite(WRITE_ENABLE, LOW);
  
  // Write data
  delayMicroseconds(20);
  for (short i = 0; i < 8; i++)  digitalWrite(DATA_PINS[i], (data >> i) & 1);
  delayMicroseconds(20);
  digitalWrite(WRITE_ENABLE, HIGH);
  delayMicroseconds(20);
}

void chip_reset() {
  write_mode();

  send_command(0x5555, 0xaa);
  send_command(0x2aaa, 0x55);
  send_command(0x5555, 0x80);
  send_command(0x5555, 0xaa);
  send_command(0x2aaa, 0x55);
  send_command(0x5555, 0x10);
  
  delay(100);  
}

void program_single(unsigned int address, byte data) {
  write_mode();

  send_command(0x5555,  0xaa);
  send_command(0x2aaa,  0x55);
  send_command(0x5555,  0xa0);
  send_command(address, data);
}

void setup() {
  pinMode(BUS_ENABLE, OUTPUT); digitalWrite(BUS_ENABLE, LOW);
  for (short i = 0; i < NBR_ADDRESS; i++) pinMode(ADDRESS_PINS_ROM[i], OUTPUT);

  pinMode(OUTPUT_ENABLE, OUTPUT); pinMode(WRITE_ENABLE, OUTPUT);
  pinMode(READ_WRITE, INPUT);
  pinMode(CHIP_ENABLE, OUTPUT); digitalWrite(CHIP_ENABLE, LOW);
  pinMode(REAL_A15, OUTPUT); digitalWrite(REAL_A15, HIGH); // pour être sûr de n'écrire sur aucun autre chip
  
  Serial.begin(BAUDRATE); 
  while (!Serial) ;
  Serial.println("Launching the flash programmer");
  Serial.println("Read mode : R  |  Write mode : W | Write mode and reset : w | Write mode and set start address to 0x8000 : x | Dump mode : D | Check mode : C");
}

void loop() {

  if (Serial.available() > 0) {
    // Setting read/write mode
    if (mode == 0) {
      received_byte = Serial.read();
      if (received_byte == 'W') mode = 2;
      else if (received_byte == 'w') {
        mode = 2;
        read_mode();
        chip_reset();
        Serial.println("Chip has been reset, ready to receive file");
      } else if (received_byte == 'x') {
        mode = 2;
        read_mode();
        chip_reset();
        
        program_single(0x7ffc, 0x00);
        program_single(0x7ffd, 0x80);
        
        Serial.println("Chip has been reset, ready to receive file");
      }
      else if (received_byte == 'D') mode = 4;
      else if (received_byte == 'C') mode = 5;
      else mode = 1;
      Serial.print("You are now in "); Serial.print((mode == 1) ? "read" : ((mode == 2) ? "write" : ((mode == 4) ? "dump" : "check"))); Serial.println(" mode");
    }

    if (mode == 1) {
      Serial.println("Start address, end address (the comma is important)");
      start_address = Serial.parseInt();
      end_address   = Serial.parseInt();
      
      if (Serial.read() == '\n') {
        read_some(start_address, end_address);
      }
    }

    if (mode == 2) {
      Serial.println("Start address, then send binary file");
      unsigned int start_address = Serial.parseInt();
      current_address = start_address;
      
      if (Serial.read() == '\n') {
        Serial.print("Start address is "); Serial.println(start_address, HEX);
        mode = 3;
      }
    } else if (mode == 3) {
      received_byte = Serial.read(); // it is possible to program it by hand, don't forget to be in "No line ending mode" if you don't want to program the '\n' at the end
      program_single(current_address, received_byte);
      current_address++;
    }

    if (mode == 4) {
      Serial.println("Start address, end address (the comma is important)");
      start_address = Serial.parseInt();
      end_address   = Serial.parseInt();

      current_address = start_address;
      if (Serial.read() == '\n') {
        dump_some(start_address, end_address);
      }
    }

    if (mode == 5) {
      Serial.println("Start address, then send binary file to compare to flash content");
      unsigned int start_address = Serial.parseInt();
      current_address = start_address;
      
      if (Serial.read() == '\n') {
        Serial.print("Start address is "); Serial.println(start_address, HEX);
        Serial.print("Program start : ");
        read_some(0x7ffc, 0x7ffe);
        Serial.println("");
        mode = 6;
        Serial.println("Ready to receive file");
      }
    } else if (mode == 6) {
      received_byte = Serial.read(); // it is possible to program it by hand, don't forget to be in "No line ending mode" if you don't want to program the '\n' at the end
      compare_some(current_address, received_byte);
      current_address++;
    }
  }
}
