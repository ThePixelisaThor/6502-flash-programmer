# 6502-flash-programmer

I didn't want to buy a flash programmer, nor did I want to pull the flash out of the breadboard each time I had to change the program.
The flash chip I am using is the 39SF010 (exact chip being the SST39SF010A), if you are using another flash chip you might be able to make it work by tweaking a few things.

## How to use

I use Yet Another Terminal [https://sourceforge.net/projects/y-a-terminal/] to send the instructions. You can send a binary file, that will be written on the flash.
When it asks for adress (or adresses), you have to send a \n at the end. For instance : 0\n, or 0,128\n

### Modes

There are different modes that can be used when resetting the arduino : 

- R for Read mode, you can read chunks of the flash
- C for Check mode, it will compare the flash content with the file you send
- W to Write on the chip (note that it doesn't reset the chip, so you can only program 0's)
- w to write with reset
- x to write with reset and set the start adress (0x7ffc and 0x7ffd) of the 6502 to 0x8000 (if you are doing Ben Eater's project it saves lots of time)

## Advantages

- No need for a flash programmer
- No need to take the flash chip out
- The arduino can also monitor
- Sets the start address, so you do not have to send 32 kB to get to the reset vector (=> it programs faster than a flash programmer)

## Limitations

To launch the 6502, you need to reprogram the arduino with the 6502_monitor script. Same the other way.

## Hardware setup

All the connections from the arduino are directly connected to the signals.

I used 1k pull-up/down resistors as follow :

Pulled-high :
- bus enable
- write enable (of the flahs)

Pulled-low:
- output enable (of the flash)
- A15 (of the flash, not the same as A15 of the address bus)

The chip enable of the flash is also directly connected to the arduino, while being connected to the output of the NAND gate via a resistor.

![Running a mastermind](images/demo.jpg)
