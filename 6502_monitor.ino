const char ADDRESS_PINS[] =     { 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, A12};
const char ADDRESS_PINS_ROM[] = { 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 18}; // of the ROM, A0 to A15
const char DATA_PINS[] = {23, 25, 27, 29, 31, 33, 35, 37};

#define NBR_ADDRESS 16   // SST39SF010A supports 17 addresses, 020A 18 addresses and 040 19 addresses. If you use more than 16, you have to abandon unsigned ints
#define WRITE_ENABLE A9  // low = enabled, write to flash
#define OUTPUT_ENABLE A8 // low = enabled, output from the flash
#define CHIP_ENABLE A7 // low = enable, flash
#define BUS_ENABLE A10 // low = disabled
#define READ_WRITE A13
#define REAL_A15 A12
#define CLOCK 2
#define BAUDRATE 2000000

void setup() {
  for (int i = 0; i < 16; i++) pinMode(ADDRESS_PINS[i], INPUT);
  for (int i = 0; i < 8; i++) pinMode(DATA_PINS[i], INPUT);
  pinMode(CLOCK, INPUT); pinMode(READ_WRITE, INPUT);
  pinMode(BUS_ENABLE, OUTPUT); digitalWrite(BUS_ENABLE, HIGH);
  
  Serial.begin(BAUDRATE);
  Serial.println("Monitoring the 6502 --- Don't forget to reset the chip");
  attachInterrupt(digitalPinToInterrupt(CLOCK), clock_triggered, FALLING);
}

void clock_triggered() {
  // Address bits
  unsigned int address = 0;
  for (int i = 0; i < 16; i++) {
    Serial.print(digitalRead(ADDRESS_PINS[i]) ? '1' : '0');
    address += digitalRead(ADDRESS_PINS[i]) << i;
  }
  Serial.print("    ");

  // Data bits
  unsigned int data = 0;
  for (int i = 0; i < 8; i++) {
    Serial.print(digitalRead(DATA_PINS[i]) ? '1' : '0');
    data += digitalRead(DATA_PINS[i]) << i;
  }
  Serial.print("    ");
  
  // Hex
  char buf[20];
  sprintf(buf, "%04x %c %02x", address, digitalRead(READ_WRITE) ? 'R' : 'W', data);
  Serial.println(buf);
}

void loop() {
  // put your main code here, to run repeatedly:

}
